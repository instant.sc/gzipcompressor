﻿// gzipCompressor
// Copyright (C) 2021  Aleksei Kunts contact@insc.cc
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;

namespace gzipCompressor
{
    //for debugging and stuff
    public class SingleThreadChunkedStreamTransformer : IChunkedStreamTransformer
    {
        public void Transform(Stream inputStream, Stream outputStream,
                              Func<RentedMemoryHolder, RentedMemoryHolder> chunkTransformer,
                              Func<Stream, RentedMemoryHolder?> chunkExtractor)
        {
            RentedMemoryHolder? sourceChunk;
            while ((sourceChunk = chunkExtractor(inputStream)) != null)
            {
                RentedMemoryHolder destinationChunk;
                using (sourceChunk)
                {
                    destinationChunk = chunkTransformer(sourceChunk.Value);
                }

                using (destinationChunk)
                {
                    outputStream.Write(destinationChunk.View.Span);
                }
            }
        }
    }
}
