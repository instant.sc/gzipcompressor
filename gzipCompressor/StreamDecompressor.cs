﻿// gzipCompressor
// Copyright (C) 2021  Aleksei Kunts contact@insc.cc
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Buffers;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace gzipCompressor
{
    public class StreamDecompressor
    {
        private readonly IChunkedStreamTransformer _streamTransformer;
        private BinaryReader _inputStreamReader;

        public StreamDecompressor(IChunkedStreamTransformer streamTransformer)
        {
            _streamTransformer = streamTransformer;
        }

        public void Decompress(Stream inputStream, Stream outputStream)
        {
            using var binaryReader = new BinaryReader(inputStream, Encoding.UTF8, true);
            _inputStreamReader = binaryReader;
            try
            {
                _streamTransformer.Transform(inputStream, outputStream, ChunkTransformer, ChunkExtractor);
            }
            finally
            {
                _inputStreamReader = null;
            }
        }

        private RentedMemoryHolder? ChunkExtractor(Stream inputStream)
        {
            var arrayPool = ArrayPool<byte>.Shared;
            int sourceLength;

            try
            {
                sourceLength = _inputStreamReader.ReadInt32();
            }
            //no good way to check for a generic stream unfortunately
            catch (EndOfStreamException)
            {
                return null;
            }

            var compressedLength = _inputStreamReader.ReadInt32();

            var chunkDataHolder = new RentedMemoryHolder(arrayPool, sizeof(int) + compressedLength);
            try
            {
                if (!BitConverter.TryWriteBytes(chunkDataHolder.View.Span, sourceLength))
                {
                    throw new Exception("Internal error: unable to store uncompressed chunk length");
                }

                var dataRead = inputStream.Read(chunkDataHolder.View.Span.Slice(sizeof(int)));
                if (dataRead != compressedLength)
                {
                    throw new Exception("Invalid block: stream ended before specified compressed size");
                }

                return chunkDataHolder;
            }
            catch (Exception ex)
            {
                chunkDataHolder.Dispose();
                throw new StreamProcessorException("An error has occurred while reading the input stream", ex);
            }
        }

        private RentedMemoryHolder ChunkTransformer(RentedMemoryHolder sourceChunk)
        {
            var arrayPool = ArrayPool<byte>.Shared;
            var uncompressedLength = BitConverter.ToInt32(sourceChunk.View.Span);

            var targetChunk = new RentedMemoryHolder(arrayPool, uncompressedLength);
            try
            {
                using var sourceStream = sourceChunk.GetMemoryStream();
                sourceStream.Position = sizeof(int);
                using var decompressStream = new GZipStream(sourceStream, CompressionMode.Decompress, true);
                var readBytes = decompressStream.Read(targetChunk.View.Span);
                if (readBytes != uncompressedLength)
                {
                    throw new Exception("Invalid block: stream ended before specified decompressed size");
                }

                return targetChunk;
            }
            catch (Exception ex)
            {
                targetChunk.Dispose();
                throw new StreamProcessorException("An error has occurred while decompressing a data chunk", ex);
            }
        }
    }
}
