﻿// gzipCompressor
// Copyright (C) 2021  Aleksei Kunts contact@insc.cc
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Buffers;
using System.IO;

namespace gzipCompressor
{
    public struct RentedMemoryHolder : IDisposable
    {
        private readonly ArrayPool<byte> _arrayPool;
        private readonly byte[] _array;
        private bool _disposed;
        public Memory<byte> View { get; private set; }

        private RentedMemoryHolder(ArrayPool<byte> arrayPool, byte[] array, int usedLength)
        {
            _arrayPool = arrayPool;
            _array = array;
            View = new Memory<byte>(_array, 0, usedLength);
            _disposed = false;
        }

        public RentedMemoryHolder(ArrayPool<byte> arrayPool, int requiredLength) :
            this(arrayPool, arrayPool.Rent(requiredLength), requiredLength)
        {
        }

        public MemoryStream GetMemoryStream()
        {
            return new MemoryStream(_array, 0, View.Length);
        }

        public void ChangeLength(int newUsedLength)
        {
            View = View.Slice(0, newUsedLength);
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                _arrayPool.Return(_array);
            }
        }
    }
}
