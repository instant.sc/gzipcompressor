﻿// gzipCompressor
// Copyright (C) 2021  Aleksei Kunts contact@insc.cc
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Buffers;
using System.IO;
using System.IO.Compression;

namespace gzipCompressor
{
    public class StreamCompressor
    {
        private const int CustomChunkHeaderSize = sizeof(int) + sizeof(int);
        private const int DeflateMultiplier = 2;
        private const int DeflateConstantModifier = 100;
        private const int GzipHeader = 18;

        private readonly IChunkedStreamTransformer _streamTransformer;
        private readonly int _chunkSize;

        public StreamCompressor(IChunkedStreamTransformer streamTransformer, int chunkSize)
        {
            if (chunkSize <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(chunkSize));
            }

            _streamTransformer = streamTransformer;
            _chunkSize = chunkSize;
        }

        public void Compress(Stream inputStream, Stream outputStream)
        {
            _streamTransformer.Transform(inputStream, outputStream, ChunkTransformer, ChunkExtractor);
        }

        private RentedMemoryHolder? ChunkExtractor(Stream inputStream)
        {
            var arrayPool = ArrayPool<byte>.Shared;
            var holder = new RentedMemoryHolder(arrayPool, _chunkSize);
            try
            {
                var actualBytesRead = inputStream.Read(holder.View.Span);
                if (actualBytesRead == 0)
                {
                    holder.Dispose();
                    return null;
                }

                holder.ChangeLength(actualBytesRead);
                return holder;
            }
            catch (Exception ex)
            {
                holder.Dispose();
                throw new StreamProcessorException("An error has occurred while reading the input stream", ex);
            }
        }

        private RentedMemoryHolder ChunkTransformer(RentedMemoryHolder sourceChunk)
        {
            var arrayPool = ArrayPool<byte>.Shared;
            var sourceChunkLength = sourceChunk.View.Length;
            //Safe buffer size for gzipped data
            //GZIP will have a header+footer of exactly 18 bytes in .net
            //(see https://stackoverflow.com/questions/31681484, https://en.wikipedia.org/wiki/Gzip)
            //Deflate itself varies, but the upper bound is normally some form of k*input_size+n, where k<2, n<100
            //e.g. in Linux kernel it is defined as s => s + ((s + 7) >> 3) + ((s + 63) >> 6) + 11
            //(see https://elixir.bootlin.com/linux/v5.0/source/include/linux/zlib.h#L508)
            //and in zlib it is sourceLen => sourceLen + ((sourceLen + 7) >> 3) + ((sourceLen + 63) >> 6) + 5
            //(see https://github.com/madler/zlib/blob/cacf7f1d4e3d44d871b605da3b647f07d718623f/deflate.c#L660)
            var targetDataHolder = new RentedMemoryHolder(arrayPool, checked(CustomChunkHeaderSize + GzipHeader + DeflateMultiplier * sourceChunkLength + DeflateConstantModifier));
            try
            {
                int writtenBytes;
                using (var targetChunkStream = targetDataHolder.GetMemoryStream())
                {
                    targetChunkStream.Position = CustomChunkHeaderSize;
                    using (var compressStream = new GZipStream(targetChunkStream, CompressionLevel.Optimal, true))
                    {
                        compressStream.Write(sourceChunk.View.Span);
                    }

                    writtenBytes = (int) targetChunkStream.Position;
                    BitConverter.TryWriteBytes(targetDataHolder.View.Span, sourceChunkLength);
                    BitConverter.TryWriteBytes(targetDataHolder.View.Span.Slice(sizeof(int)), writtenBytes - CustomChunkHeaderSize);
                }

                targetDataHolder.ChangeLength(writtenBytes);
                return targetDataHolder;
            }
            catch (Exception ex)
            {
                targetDataHolder.Dispose();
                throw new StreamProcessorException("An error has occurred while compressing a data chunk", ex);
            }
        }
    }
}
