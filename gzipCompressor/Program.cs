﻿// gzipCompressor
// Copyright (C) 2021  Aleksei Kunts contact@insc.cc
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.CommandLine;
using System.CommandLine.Builder;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.IO;

namespace gzipCompressor
{
    class Program
    {
        static void Main(string[] args)
        {
            var threadsOption = new Option<int>("--threads", () => Environment.ProcessorCount, "Number of compression/decompression threads");
            threadsOption.AddValidator(result =>
            {
                var value = result.GetValueOrDefault<int>();
                if (value > Environment.ProcessorCount)
                {
                    Console.WriteLine($"[Warn] Thread count is set to {value}, which is more than the recommended value of {Environment.ProcessorCount}");
                }

                if (value < 0)
                {
                    return "Thread count must be non-negative";
                }

                return null;
            });
            var chunkSizeOption = new Option<int>(new[] { "--chunk-size", "-c" }, () => 1_000_000, "Compressed chunk size")
            {
            };
            chunkSizeOption.AddValidator(result =>
            {
                var value = result.GetValueOrDefault<int>();
                if (value <= 0)
                {
                    return "Chunk size must be positive";
                }

                const int maxChunkSize = 1_000_000_000;
                if (value > maxChunkSize)
                {
                    return $"Chunks of size above {maxChunkSize} are not supported";
                }

                return null;
            });

            var forceOption = new Option<bool>(new[] { "--force", "-f" }, () => false, "Overwrite output file without asking");
            var inputFileArgument = new Argument<FileInfo>("inputFile", "Path to the input file").LegalFilePathsOnly().ExistingOnly();
            var outputFileArgument = new Argument<FileInfo>("outputFile", "Path to the output file").LegalFilePathsOnly();
            outputFileArgument.AddValidator(result =>
            {
                var outputFile = result.GetValueOrDefault<FileInfo>();
                if (!outputFile.Directory.Exists)
                {
                    return "Output file directory does not exist";
                }

                return null;
            });

            var compressCommand = new Command("compress")
            {
                inputFileArgument,
                outputFileArgument
            };
            compressCommand.AddOption(chunkSizeOption);
            compressCommand.Handler = CommandHandler.Create<FileInfo, FileInfo, int, int, bool>((inputFile, outputFile, threads, chunkSize, force) =>
            {
                force = ValidateExistingOutputFile(force, outputFile);
                Utility.AbortOnException(() => Utility.Compress(inputFile, outputFile, force, threads, chunkSize));
            });

            var decompressCommand = new Command("decompress")
            {
                inputFileArgument,
                outputFileArgument
            };
            decompressCommand.Handler = CommandHandler.Create<FileInfo, FileInfo, int, bool>((inputFile, outputFile, threads, force) =>
            {
                force = ValidateExistingOutputFile(force, outputFile);
                Utility.AbortOnException(() => Utility.Decompress(inputFile, outputFile, force, threads));
            });

            var rootCommand = new RootCommand { Description = "My sample app" };
            rootCommand.AddGlobalOption(threadsOption);
            rootCommand.AddGlobalOption(forceOption);
            rootCommand.AddCommand(compressCommand);
            rootCommand.AddCommand(decompressCommand);
            rootCommand.AddValidator(_ => "Please invoke one of the subcommands");

            var parser = new CommandLineBuilder(rootCommand).UseVersionOption()
                                                            .UseHelp()
                                                            .RegisterWithDotnetSuggest()
                                                            .UseTypoCorrections()
                                                            .UseParseErrorReporting()
                                                            .UseExceptionHandler()
                                                            .CancelOnProcessTermination()
                                                            .Build();

            var parseResult = parser.Parse(args);
            parseResult.Invoke();
        }

        private static bool ValidateExistingOutputFile(bool force, FileInfo file)
        {
            if (!force)
            {
                if (file.Exists)
                {
                    Console.WriteLine($"Are you sure you want to overwrite the file {file.FullName}? (y/n)");
                    if (Console.ReadLine() != "y")
                    {
                        Console.WriteLine("Aborted");
                        Environment.Exit(1);
                    }

                    return true;
                }
            }

            return force;
        }
    }
}
