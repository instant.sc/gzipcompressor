﻿// gzipCompressor
// Copyright (C) 2021  Aleksei Kunts contact@insc.cc
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;

namespace gzipCompressor
{
    public static class Utility
    {
        public static void Compress(FileInfo input, FileInfo output, bool force, int threads, int chunkSize)
        {
            using var inputStream = input.OpenRead();
            using var outputStream = output.Open(force ? FileMode.Create : FileMode.CreateNew, FileAccess.Write);
            var compressor = new StreamCompressor(new ParallelChunkedStreamTransformer(threads), chunkSize);
            compressor.Compress(inputStream, outputStream);
        }

        public static void Decompress(FileInfo input, FileInfo output, bool force, int threads)
        {
            using var inputStream = input.OpenRead();
            using var outputStream = output.Open(force ? FileMode.Create : FileMode.CreateNew, FileAccess.Write);
            var compressor = new StreamDecompressor(new ParallelChunkedStreamTransformer(threads));
            compressor.Decompress(inputStream, outputStream);
        }

        public static void AbortOnException(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetFullMessage());
                Environment.Exit(1);
            }
        }
    }
}
