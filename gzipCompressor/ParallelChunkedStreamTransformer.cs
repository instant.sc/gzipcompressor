﻿// gzipCompressor
// Copyright (C) 2021  Aleksei Kunts contact@insc.cc
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Threading;

namespace gzipCompressor
{
    public class ParallelChunkedStreamTransformerData
    {
        public readonly ConcurrentQueue<(long, RentedMemoryHolder)> InputChunkQueue = new();
        public readonly Semaphore InputChunkSemaphore = new(0, int.MaxValue);
        public readonly ManualResetEvent QueueEmptyEvent = new(true);
        public readonly ConcurrentQueue<(long, RentedMemoryHolder)> OutputChunkQueue = new();
        public readonly Semaphore OutputChunkSemaphore = new(0, int.MaxValue);
        public readonly SortedDictionary<long, RentedMemoryHolder> OutputChunkSortedDictionary = new();

        public readonly ConcurrentBag<Exception> Exceptions = new();
        public bool InputFinished;
        public bool ProcessingFinished;
        public bool HasErrors;
    }

    public class ParallelChunkedStreamTransformer : IChunkedStreamTransformer
    {
        private readonly int _maxThreads;

        public ParallelChunkedStreamTransformer(int maxThreads)
        {
            _maxThreads = maxThreads;
        }

        private void InputThread(ParallelChunkedStreamTransformerData data, Stream inputStream, Func<Stream, RentedMemoryHolder?> chunkExtractor)
        {
            long nextInputChunkId = 0;
            while (!data.HasErrors)
            {
                if (!data.QueueEmptyEvent.WaitOne(1000))
                {
                    continue;
                }

                data.QueueEmptyEvent.Reset();
                while (!data.HasErrors && data.InputChunkQueue.Count < _maxThreads * 2)
                {
                    var newChunk = chunkExtractor(inputStream);
                    if (newChunk == null)
                    {
                        return;
                    }

                    data.InputChunkQueue.Enqueue((nextInputChunkId++, newChunk.Value));
                    data.InputChunkSemaphore.Release();
                }
            }
        }

        private void OutputThread(ParallelChunkedStreamTransformerData data, Stream outputStream)
        {
            long nexOutputChunkId = 0;
            while (!data.HasErrors)
            {
                if (!data.OutputChunkSemaphore.WaitOne(1000))
                {
                    continue;
                }

                if (!data.OutputChunkQueue.TryDequeue(out var chunkPair))
                {
                    if (data.ProcessingFinished)
                    {
                        return;
                    }

                    throw new Exception("Internal error: output chunk queue was empty");
                }

                data.OutputChunkSortedDictionary.Add(chunkPair.Item1, chunkPair.Item2);
                while (!data.HasErrors && data.OutputChunkSortedDictionary.ContainsKey(nexOutputChunkId))
                {
                    data.OutputChunkSortedDictionary.Remove(nexOutputChunkId++, out var nextChunk);
                    using (nextChunk)
                    {
                        try
                        {
                            outputStream.Write(nextChunk.View.Span);
                        }
                        catch (Exception ex)
                        {
                            throw new StreamProcessorException("An error has occurred while writing the output stream", ex);
                        }
                    }
                }
            }
        }

        private void ProcessingThread(ParallelChunkedStreamTransformerData data, Func<RentedMemoryHolder, RentedMemoryHolder> chunkTransformer)
        {
            while (!data.HasErrors)
            {
                if (!data.InputChunkSemaphore.WaitOne(1000))
                {
                    continue;
                }


                if (!data.InputChunkQueue.TryDequeue(out var chunkPair))
                {
                    if (data.InputFinished)
                    {
                        return;
                    }

                    throw new Exception("Internal error: input chunk queue was empty");
                }

                if (data.InputChunkQueue.Count < _maxThreads * 2)
                {
                    data.QueueEmptyEvent.Set();
                }

                using (chunkPair.Item2)
                {
                    var transformedChunk = chunkTransformer(chunkPair.Item2);
                    data.OutputChunkQueue.Enqueue((chunkPair.Item1, transformedChunk));
                    data.OutputChunkSemaphore.Release();
                }
            }
        }

        private void HandleExceptions(ParallelChunkedStreamTransformerData data, Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                data.HasErrors = true;
                data.Exceptions.Add(ex);
            }
        }

        public void Transform(Stream inputStream, Stream outputStream,
                              Func<RentedMemoryHolder, RentedMemoryHolder> chunkTransformer,
                              Func<Stream, RentedMemoryHolder?> chunkExtractor)
        {
            var transformData = new ParallelChunkedStreamTransformerData();
            try
            {
                var iThr = new Thread(() => HandleExceptions(transformData, () => InputThread(transformData, inputStream, chunkExtractor)))
                {
                    Name = "InputThread"
                };
                iThr.Start();
                var processingThreads = Enumerable
                                       .Repeat(0, _maxThreads)
                                       .Select((_, i) =>
                                                   new Thread(() => HandleExceptions
                                                                  (transformData,
                                                                   () => ProcessingThread(transformData, chunkTransformer)))
                                                   {
                                                       Name = $"ProcessingThread{i}"
                                                   })
                                       .ToArray();
                foreach (var thread in processingThreads)
                {
                    thread.Start();
                }

                var oThr = new Thread(() => HandleExceptions(transformData, () => OutputThread(transformData, outputStream)))
                {
                    Name = "OutputThread"
                };
                oThr.Start();
                try
                {
                    iThr.Join();
                }
                finally
                {
                    transformData.InputFinished = true;
                    transformData.InputChunkSemaphore.Release(_maxThreads);
                }

                try
                {
                    foreach (var thread in processingThreads)
                    {
                        thread.Join();
                    }
                }
                finally
                {
                    transformData.ProcessingFinished = true;
                    transformData.OutputChunkSemaphore.Release();
                }

                oThr.Join();
            }
            catch (Exception ex)
            {
                transformData.HasErrors = true;
                transformData.Exceptions.Add(ex);
            }
            finally
            {
                FinalizeProcessing(transformData);
            }
        }

        private void FinalizeProcessing(ParallelChunkedStreamTransformerData transformData)
        {
            bool hasUnprocessedItems = !transformData.InputChunkQueue.IsEmpty ||
                                       !transformData.OutputChunkQueue.IsEmpty ||
                                       transformData.OutputChunkSortedDictionary.Count != 0;
            if (hasUnprocessedItems)
            {
                while (transformData.InputChunkQueue.TryDequeue(out var inputChunk))
                {
                    inputChunk.Item2.Dispose();
                }

                while (transformData.OutputChunkQueue.TryDequeue(out var outputChunk))
                {
                    outputChunk.Item2.Dispose();
                }

                foreach (var holder in transformData.OutputChunkSortedDictionary)
                {
                    holder.Value.Dispose();
                }

                if (!transformData.HasErrors)
                {
                    throw new ParallelChunkStreamTransformerException("Internal error: all transformations completed, but internal queues contain unprocessed items");
                }
            }

            if (transformData.HasErrors)
            {
                if (transformData.Exceptions.Count == 1)
                {
                    ExceptionDispatchInfo.Throw(transformData.Exceptions.Single());
                }

                throw new AggregateException("Multiple errors occurred", transformData.Exceptions);
            }
        }
    }
}
