// gzipCompressor
// Copyright (C) 2021  Aleksei Kunts contact@insc.cc
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.IO;
using gzipCompressor;
using NUnit.Framework;

[assembly: LevelOfParallelism(16)]

namespace gzipCompressorTests
{
    public class Tests
    {
        [TestCase(1_000_000_000, 1_000_000, 1)]
        [TestCase(1_000_000_000, 1_000_000, 2)]
        [TestCase(1_000_000_000, 1_000_000, 4)]
        [TestCase(1_000_000_000, 1_000_000, 8)]
        [TestCase(1_000_000_000, 1_000_000, 16)]
        public void CompressDecompressStream(int length, int chunkSize, int parallel = 1)
        {
            var compressor = new StreamCompressor(new ParallelChunkedStreamTransformer(parallel), chunkSize);
            var decompressor = new StreamDecompressor(new ParallelChunkedStreamTransformer(parallel));
            var inputOutputStream = new ByteGeneratorStream(1333, length);
            var intermediateStream = new MemoryStream();
            compressor.Compress(inputOutputStream, intermediateStream);
            intermediateStream.Position = 0;
            decompressor.Decompress(intermediateStream, inputOutputStream);
            Assert.IsTrue(inputOutputStream.IsWriteAmountCorrect());
        }

        [Test]
        public void CantReadError()
        {
            var compressor = new StreamCompressor(new ParallelChunkedStreamTransformer(1), 100);
            var inputOutputStream = new FailingReadStream(1333, 1000);
            var intermediateStream = new MemoryStream();
            var exception = Assert.Throws<StreamProcessorException>(() => compressor.Compress(inputOutputStream, intermediateStream));
            Assert.AreEqual("An error has occurred while reading the input stream --> Can't read", exception.GetFullMessage());
        }

        [Test]
        public void CantWriteError()
        {
            var compressor = new StreamCompressor(new ParallelChunkedStreamTransformer(1), 100);
            var decompressor = new StreamDecompressor(new ParallelChunkedStreamTransformer(1));
            var inputOutputStream = new FailingWriteStream(1333, 1000);
            var intermediateStream = new MemoryStream();
            compressor.Compress(inputOutputStream, intermediateStream);
            intermediateStream.Position = 0;
            var exception = Assert.Throws<StreamProcessorException>(() => decompressor.Decompress(intermediateStream, inputOutputStream));
            Assert.AreEqual("An error has occurred while writing the output stream --> Can't write", exception.GetFullMessage());
        }
    }
}
