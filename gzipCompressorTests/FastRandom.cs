﻿// gzipCompressor
// Copyright (C) 2021  Aleksei Kunts contact@insc.cc
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Runtime.InteropServices;

namespace gzipCompressorTests
{
    public class FastRandom
    {
        private uint _x;
        private uint _y;
        private uint _z;
        private uint _w;

        public FastRandom(int seed)
        {
            var init = new Random(seed);
            _x = (uint) init.Next();
            _y = (uint) init.Next();
            _z = (uint) init.Next();
            _w = (uint) init.Next();
        }

        public void NextBytes(Span<byte> bytes)
        {
            if (bytes.Length % (sizeof(uint) / sizeof(byte)) != 0)
            {
                throw new NotSupportedException();
            }

            var uintSpan = MemoryMarshal.Cast<byte, uint>(bytes);
            FillBuffer(uintSpan);
        }

        private void FillBuffer(Span<uint> buffer)
        {
            int index = 0;
            uint x = _x;
            uint y = _y;
            uint z = _z;
            uint w = _w;
            while (index < buffer.Length)
            {
                uint t = x ^ (x << 11);
                x = y;
                y = z;
                z = w;
                w = w ^ (w >> 19) ^ t ^ (t >> 8);
                buffer[index++] = w;
            }

            _x = x;
            _y = y;
            _z = z;
            _w = w;
        }
    }
}
