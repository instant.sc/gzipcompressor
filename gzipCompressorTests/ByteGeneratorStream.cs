﻿// gzipCompressor
// Copyright (C) 2021  Aleksei Kunts contact@insc.cc
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Buffers;
using System.IO;

namespace gzipCompressorTests
{
    public class ByteGeneratorStream : Stream
    {
        private readonly int _seed;
        private readonly int _length;
        private readonly FastRandom _writeRandom;
        private readonly FastRandom _readRandom;
        private int _writePosition;
        private int _readPosition;

        public ByteGeneratorStream(int seed, int length)
        {
            _seed = seed;
            _length = length;
            _readRandom = new FastRandom(_seed);
            _writeRandom = new FastRandom(_seed);
        }

        public override void Flush()
        {
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (buffer.Length - offset < count)
            {
                throw new ArgumentException();
            }

            var actualCount = Math.Min(count, _length - _readPosition);
            _readRandom.NextBytes(buffer.AsSpan(offset, actualCount));
            _readPosition += actualCount;
            return actualCount;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (buffer.Length - offset < count)
            {
                throw new ArgumentException();
            }

            if (_length - _writePosition < count)
            {
                throw new Exception("Too many input bytes!");
            }

            var randomBytes = ArrayPool<byte>.Shared.Rent(count);
            try
            {
                var randomBytesSpan = randomBytes.AsSpan(0, count);
                _writeRandom.NextBytes(randomBytesSpan);
                _writePosition += count;
                if (!randomBytesSpan.SequenceEqual(buffer.AsSpan(offset, count)))
                {
                    throw new Exception("Incorrect write content");
                }
            }
            finally
            {
                ArrayPool<byte>.Shared.Return(randomBytes);
            }
        }

        public bool IsWriteAmountCorrect()
        {
            return _writePosition == _length;
        }

        public override bool CanRead => true;
        public override bool CanSeek => false;
        public override bool CanWrite => true;
        public override long Length => throw new NotSupportedException();

        public override long Position
        {
            get => throw new NotSupportedException();
            set => throw new NotSupportedException();
        }
    }
}
